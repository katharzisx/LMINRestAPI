package lmin.core.services;

import java.io.File;
import java.io.FileNotFoundException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.PropertyConfigurator;

@WebListener
public class BaseServletContextListener implements ServletContextListener{
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		System.out.println("System Starting up!");

		try {
			// initialize log4j
			ServletContext context = servletContextEvent.getServletContext();
			String log4jConfigFile = context.getInitParameter("log4j-config-location");
			String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
			PropertyConfigurator.configure(fullPath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		System.out.println("System Shutting down!");
	}
}
package lmin.core.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.google.gson.Gson;

import lmin.core.models.DocumentData;
import lmin.core.models.MessageResponse;

/**
 * Servlet implementation class DocumentDataServlet
 */
@MultipartConfig //Required to handle multipartform requests
@WebServlet(name="DocumentDataServlet", displayName="DocumentDataServlet", urlPatterns = {"/storage/documents", "/storage/documents/*"}, loadOnStartup = 2)
public class DocumentDataServlet extends HttpServlet implements BaseServletInterface{
	private static final long serialVersionUID = 1L;
	//In memory DB reference
	ConcurrentHashMap<String, ConcurrentHashMap<String, Object>> mapInMemoryDB = null;
	//ENUM defining service types
	public enum SERVICE_REQUEST_TYPES{
		GET_DOCUMENT_BY_ID,
		POST_DOCUMENT,
		PUT_DOCUMENT,
		DELETE_DOCUMENT,
		NONE
	}

	public enum RESPONSE_TYPES{
		ERROR,
		POST_DOCUMENT,
		PUT_DOCUMENT,
		DELETE_DOCUMENT,
		NONE
	}
	public enum MESSAGE_RESPONSES{
		URL_INCORRECT("Request URL incorrect!", 1),
		DOCUMENT_DATABASE_EMPTY("No data in database!", 2),
		DOCUMENT_DATA_DOES_NOT_EXIST("The requested document does not exist in database!", 3),
		DOCUMENT_CREATED("Document created!", 4),
		DOCUMENT_CREATION_ERROR("Document could not be created!", 5),
		DOCUMENT_DELETED("Document deleted!", 6),
		DOCUMENT_UPDATED("Document updated!", 7),
		DOCUMENT_ID_INCORRECT_REQUEST("Document ID provided is Incorrect(lenght 20 char)!", 8);

		private String message;
		private int code;
		public String message() {
			return message;
		}
		public int code() {
			return code;
		}
		MESSAGE_RESPONSES(String msg, int code){
			this.message = msg;
			this.code = code;
		}
	}

	String RESPONSE_TYPE_JSON = "application/json";
	String RESPONSE_TYPE_TEXT = "text/plain; charset=UTF-8";
	String RESPONSE_TYPE_PDF = "application/pdf";

	//String Constants
	String POST_DOCUMENT_URI_PATH = "DOCUMENTS";



	public DocumentDataServlet() {
		super();  
	}


	public void init(ServletConfig config) throws ServletException {
		//Get reference to InMemory Datastore
		mapInMemoryDB = DATA_SOURCE.getDBAccess();
	}


	/**
	 * METHOD TO consume GET Requests
	 * SAMPLE URL: http://localhost:8080/LMINRestAPI/storage/documents/4WD6O3Q9WCQEBG6AH5O3
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType(RESPONSE_TYPE_TEXT);
		//RESPONSE DATA
		DocumentData retrievedDocument = null;
		Boolean isDocIdIncorrectLength = false;
		Boolean isDocTypeTEXT = false;
		String responseData = "";
		String documentIDRequestParam = "";
		MessageResponse messageResponse = null;
		//Stream/Readers
		ServletOutputStream streamResponse = null;
		BufferedInputStream bufFileRead = null;
		//Service request type
		SERVICE_REQUEST_TYPES request_type = SERVICE_REQUEST_TYPES.NONE;
		Gson gson = new Gson();
		String requestURI = request.getRequestURI().substring(request.getContextPath().length());
		String[] uriData = requestURI.split("/");
		int len = uriData.length;
		//if there are more than 3 items then url is not allowed
		UTIL_HANDLER.serverlog("[GET]URI DATA LEN"+len);
		UTIL_HANDLER.serverlog("[GET]URI DATA "+uriData[len-1]);

		if(len==4) {
			request_type = SERVICE_REQUEST_TYPES.GET_DOCUMENT_BY_ID;
			documentIDRequestParam = uriData[len-1];
		}else {
			request_type = SERVICE_REQUEST_TYPES.NONE;
		}

		//Check if Document ID is of incorrect length (CORRECT = 20)
		if(request_type ==SERVICE_REQUEST_TYPES.GET_DOCUMENT_BY_ID  && documentIDRequestParam.length()!=20) {
			request_type = SERVICE_REQUEST_TYPES.NONE;
			isDocIdIncorrectLength = true;
		}

		UTIL_HANDLER.serverlog("REQUEST TYPE:"+request_type.toString());

		//assign appropriate process methods
		switch (request_type) {
		case GET_DOCUMENT_BY_ID:
			retrievedDocument = processGetDocumentByID(documentIDRequestParam);
			if(retrievedDocument!=null) {
				String textContentType = "TEXT";
				String multipartContentType = "MULTIPART";
				if(retrievedDocument.getContent_type().toUpperCase().contains(textContentType)) {
					isDocTypeTEXT = true;
					UTIL_HANDLER.serverlog("[GET] the file type to send is TEXT");
					response.setContentType(RESPONSE_TYPE_TEXT);
					//fetch the string content from the file
					responseData = retrievedDocument.getText_content_data();
				}else if(retrievedDocument.getContent_type().toUpperCase().contains(multipartContentType)) {
					isDocTypeTEXT = false;
					UTIL_HANDLER.serverlog("[GET] the file type to send is: "+retrievedDocument.getContent_type());
					response.setContentType(retrievedDocument.getContent_type());
					response.addHeader("Content-Disposition", "attachment; filename="
							+ retrievedDocument.getFilename());
					//Convert to stream and send
					try {

						//get the output stream
						streamResponse = response.getOutputStream();
						bufFileRead = new BufferedInputStream(retrievedDocument.getDoc_file());
						int readBytes = 0;
						//read from the file; write to the ServletOutputStream
						while ((readBytes = bufFileRead.read()) != -1) {
							streamResponse.write(readBytes);
						}
					} catch (IOException ioe) {
						throw new ServletException(ioe.getMessage());
					} finally {
						if (streamResponse != null)
							streamResponse.close();
						if (bufFileRead != null)
							bufFileRead.close();
					}

				}
			}else {
				//SET RESPONSE STATUS CODE
				response.setStatus(404);

				messageResponse = new MessageResponse(MESSAGE_RESPONSES.DOCUMENT_DATA_DOES_NOT_EXIST.message, MESSAGE_RESPONSES.DOCUMENT_DATA_DOES_NOT_EXIST.code);
				responseData = gson.toJson(messageResponse);
			}

			break;
		case NONE:
			//De-serialize and send response
			response.setContentType(RESPONSE_TYPE_JSON);
			if(!isDocIdIncorrectLength) {
				messageResponse = new MessageResponse(MESSAGE_RESPONSES.URL_INCORRECT.message, MESSAGE_RESPONSES.URL_INCORRECT.code);
				responseData = gson.toJson(messageResponse);
			}else {
				messageResponse = new MessageResponse(MESSAGE_RESPONSES.DOCUMENT_ID_INCORRECT_REQUEST.message, MESSAGE_RESPONSES.DOCUMENT_ID_INCORRECT_REQUEST.code);
				responseData = gson.toJson(messageResponse);
			}

			break;
		default:
			break;
		}
		UTIL_HANDLER.serverlog("Response type TEXT status:"+isDocTypeTEXT);
		UTIL_HANDLER.serverlog(""+responseData);
		//		if(isDocTypeTEXT || request_type == SERVICE_REQUEST_TYPES.NONE) {
		//			response.getOutputStream().print(responseData);
		//			response.getOutputStream().flush();
		//		}

		response.getOutputStream().print(responseData);
		response.getOutputStream().flush();
	}



	/**
	 * Method to consume POST requests
	 * Sample URL: http://localhost:8080/LMINRestAPI/storage/documents
	 * TEXT BODY
	 * Response:
	 * {
		    "message": "Document created! DOC ID: FL4K2DO8DOIVUMWGY5QN",
		    "messageCode": 4
		}
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Set the response message's MIME type
		response.setContentType(RESPONSE_TYPE_JSON);  
		//RESPONSE DATA
		String responseData = "";
		MessageResponse messageResponse = null;
		//Service request type
		SERVICE_REQUEST_TYPES request_type = SERVICE_REQUEST_TYPES.NONE;
		Gson gson = new Gson();
		String requestURI = request.getRequestURI().substring(request.getContextPath().length());
		String[] uriData = requestURI.split("/");
		int len = uriData.length;
		//if there are more than 3 items then url is not allowed
		UTIL_HANDLER.serverlog("[POST]URI DATA LEN"+len);
		UTIL_HANDLER.serverlog("[POST]URI DATA "+uriData[len-1]);

		if(len==3 && uriData[len-1].equalsIgnoreCase(POST_DOCUMENT_URI_PATH)) {
			request_type = SERVICE_REQUEST_TYPES.POST_DOCUMENT;
		}else {
			request_type = SERVICE_REQUEST_TYPES.NONE;
		}

		UTIL_HANDLER.serverlog("REQUEST TYPE:"+request_type.toString());

		//assign appropriate process methods
		switch (request_type) {
		case POST_DOCUMENT:
			String documentID = processDocumentUploadRequest(request);
			if(documentID!=null) {
				//RESPONSE STATUS CODE 201: CREATED RESOURCE
				response.setStatus(201);

				messageResponse = new MessageResponse(MESSAGE_RESPONSES.DOCUMENT_CREATED.message +" DOC ID: "+documentID, MESSAGE_RESPONSES.DOCUMENT_CREATED.code);
			}else {
				messageResponse = new MessageResponse(MESSAGE_RESPONSES.DOCUMENT_CREATION_ERROR.message, MESSAGE_RESPONSES.DOCUMENT_CREATION_ERROR.code);
			}
			//if false
			responseData = gson.toJson(messageResponse);
			break;
		case NONE:
			//RESPONSE STATUS CODE 400: BAD REQUEST
			response.setStatus(400);
			//De-serialize and send response
			messageResponse = new MessageResponse(MESSAGE_RESPONSES.URL_INCORRECT.message, MESSAGE_RESPONSES.URL_INCORRECT.code);
			responseData = gson.toJson(messageResponse);
			break;
		default:
			break;
		}

		UTIL_HANDLER.serverlog("Response:");
		UTIL_HANDLER.serverlog(""+responseData);
		response.getOutputStream().print(responseData);
		response.getOutputStream().flush();

	}

	/**
	 * Method to update doc body text
	 * Sample PUT url: 
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String documentIDRequestParam = null;
		Boolean isDocIdIncorrectLength = false;
		//RESPONSE DATA
		String responseData = "";
		MessageResponse messageResponse = null;
		SERVICE_REQUEST_TYPES request_type = SERVICE_REQUEST_TYPES.NONE;
		Gson gson = new Gson();
		String requestURI = request.getRequestURI().substring(request.getContextPath().length());
		String[] uriData = requestURI.split("/");
		int len = uriData.length;
		//if there are more than 3 items then url is not allowed
		UTIL_HANDLER.serverlog("[PUT]URI DATA LEN"+len);
		UTIL_HANDLER.serverlog("[PUT]URI DATA "+uriData[len-1]);

		if(len==4) {
			request_type = SERVICE_REQUEST_TYPES.PUT_DOCUMENT;
			documentIDRequestParam = uriData[len-1];
		}else {
			request_type = SERVICE_REQUEST_TYPES.NONE;
		}

		//Check if Document ID is of incorrect length (CORRECT = 20)
		if(request_type ==SERVICE_REQUEST_TYPES.PUT_DOCUMENT  && documentIDRequestParam.length()!=20) {
			request_type = SERVICE_REQUEST_TYPES.NONE;
			isDocIdIncorrectLength = true;
			UTIL_HANDLER.serverlog("Doc ID to Updated(PUT): "+documentIDRequestParam);
		}

		UTIL_HANDLER.serverlog("REQUEST TYPE:"+request_type.toString());

		switch (request_type) {
		case PUT_DOCUMENT:
			//Check content type to update
			if(request.getContentType().contains("text/plain")) {
				String msgbody = request.getReader().lines().collect(Collectors.joining());
				UTIL_HANDLER.serverlog("[UPDATE] REQUEST BODY DATA:"+msgbody);
				//CHECK IF DOC ID EXISTS
				if(mapInMemoryDB!=null && mapInMemoryDB.size()>0) {
					ConcurrentHashMap<String, Object> mapDocumentTable = mapInMemoryDB.get(DATA_SOURCE.DOCUMENT_TABLE);
					if(mapDocumentTable.containsKey(documentIDRequestParam)) {
						DocumentData retrievedDocumentFromDB = (DocumentData)mapDocumentTable.get(documentIDRequestParam);
						String oldData = retrievedDocumentFromDB.getText_content_data();
						UTIL_HANDLER.serverlog("[UPDATE] OLD FILE DATA:"+oldData);
						//SET THE NEW BODY
						retrievedDocumentFromDB.setText_content_data(msgbody);
						String updatedData = retrievedDocumentFromDB.getText_content_data();
						UTIL_HANDLER.serverlog("[UPDATE] UPDATED FILE DATA:"+oldData);
						UTIL_HANDLER.serverlog("[UPDATE]: UPDATED DOC WITH ID:"+documentIDRequestParam);
						//SET RESPONSE STATUS: DOC updated
						response.setStatus(204);

					}else {
						//document not found
						//SET RESPONSE STATUS 404 resource not found
						response.setStatus(404);
						UTIL_HANDLER.serverlog("[UPDATE]: NOT FOUND DOC WITH ID:"+documentIDRequestParam);
					}
				}else {
					//SET RESPONSE STATUS 404 resource not found
					response.setStatus(404);
					UTIL_HANDLER.serverlog("[UPDATE]: NOT FOUND DOC WITH ID:"+documentIDRequestParam);
				}

			}else {
				//RESPONSE STATUS CODE 400: BAD REQUEST
				response.setStatus(400);
				//logic for file update
			}

			break;
		case NONE:
			//RESPONSE STATUS CODE 400: BAD REQUEST
			response.setStatus(400);
			//De-serialize and send response
			messageResponse = new MessageResponse(MESSAGE_RESPONSES.URL_INCORRECT.message, MESSAGE_RESPONSES.URL_INCORRECT.code);
			responseData = gson.toJson(messageResponse);
			break;

		default:
			break;
		}

		UTIL_HANDLER.serverlog("Response:");
		UTIL_HANDLER.serverlog(""+responseData);
		response.getOutputStream().print(responseData);
		response.getOutputStream().flush();


	}

	/**
	 * DO DELETE
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String documentIDRequestParam = null;
		Boolean isDocIdIncorrectLength = false;
		//RESPONSE DATA
		String responseData = "";
		MessageResponse messageResponse = null;
		SERVICE_REQUEST_TYPES request_type = SERVICE_REQUEST_TYPES.NONE;
		Gson gson = new Gson();
		String requestURI = request.getRequestURI().substring(request.getContextPath().length());
		String[] uriData = requestURI.split("/");
		int len = uriData.length;
		//if there are more than 3 items then url is not allowed
		UTIL_HANDLER.serverlog("[DELETE]URI DATA LEN"+len);
		UTIL_HANDLER.serverlog("[DELETE]URI DATA "+uriData[len-1]);

		if(len==4) {
			request_type = SERVICE_REQUEST_TYPES.DELETE_DOCUMENT;
			documentIDRequestParam = uriData[len-1];
		}else {
			request_type = SERVICE_REQUEST_TYPES.NONE;
		}

		//Check if Document ID is of incorrect length (CORRECT = 20)
		if(request_type ==SERVICE_REQUEST_TYPES.DELETE_DOCUMENT  && documentIDRequestParam.length()!=20) {
			request_type = SERVICE_REQUEST_TYPES.NONE;
			isDocIdIncorrectLength = true;
			UTIL_HANDLER.serverlog("Doc ID to Delete(Delete): "+documentIDRequestParam);
		}

		UTIL_HANDLER.serverlog("REQUEST TYPE:"+request_type.toString());
		
		switch (request_type) {
		case DELETE_DOCUMENT:
			//CHECK IF DOC ID EXISTS
			if(mapInMemoryDB!=null && mapInMemoryDB.size()>0) {
				ConcurrentHashMap<String, Object> mapDocumentTable = mapInMemoryDB.get(DATA_SOURCE.DOCUMENT_TABLE);
				if(mapDocumentTable.containsKey(documentIDRequestParam)) {
					//REMOVE DOC WITH ID
					mapDocumentTable.remove(documentIDRequestParam);
					//SET RESPONSE STATUS: DOC DELETED
					response.setStatus(204);
					UTIL_HANDLER.serverlog("[DELETE]: DELETED DOC WITH ID:"+documentIDRequestParam);

				}else {
					//document not found
					//SET RESPONSE STATUS 404 resource not found
					response.setStatus(404);
					UTIL_HANDLER.serverlog("[DELETE]:NOT FOUND DOC WITH ID:"+documentIDRequestParam);
				}
			}else {
				//SET RESPONSE STATUS 404 resource not found
				response.setStatus(404);
				UTIL_HANDLER.serverlog("[DELETE]:NOT FOUND DOC WITH ID:"+documentIDRequestParam);
			}
			break;
		case NONE:
			//RESPONSE STATUS CODE 400: BAD REQUEST
			response.setStatus(400);
			//De-serialize and send response
			messageResponse = new MessageResponse(MESSAGE_RESPONSES.URL_INCORRECT.message, MESSAGE_RESPONSES.URL_INCORRECT.code);
			responseData = gson.toJson(messageResponse);
			break;

		default:
			break;
		}
		
		UTIL_HANDLER.serverlog("Response:");
		UTIL_HANDLER.serverlog(""+responseData);
		response.getOutputStream().print(responseData);
		response.getOutputStream().flush();
	}


	/**
	 * Get doc by ID
	 * @param documentID
	 * @return
	 */
	private DocumentData processGetDocumentByID(String documentID) {
		DocumentData retrievedDocumentFromDB = null;
		if(mapInMemoryDB!=null && mapInMemoryDB.size()>0) {
			ConcurrentHashMap<String, Object> mapDocumentTable = mapInMemoryDB.get(DATA_SOURCE.DOCUMENT_TABLE);
			if(mapDocumentTable!=null && mapDocumentTable.size()>0) {
				if(mapDocumentTable.containsKey(documentID)) {
					retrievedDocumentFromDB = (DocumentData)mapDocumentTable.get(documentID);
				}else {
					//document not found
				}
			}else {
				//Document table does not exist
			}
		}else {
			//DB or Table does not exist
		}
		return retrievedDocumentFromDB;
	}

	/**
	 * Upload doc
	 * @param request
	 * @return
	 */
	public String processDocumentUploadRequest(HttpServletRequest request) {

		String newDocumentID = null;

		UTIL_HANDLER.serverlog("[processDocumentUploadRequest]");
		try {


			String requestContentType = request.getContentType();
			UTIL_HANDLER.serverlog("[processDocumentUploadRequest] REQUEST CONTENT TYPE:"+requestContentType);


			if(requestContentType.contains("text/plain")) {
				//add new document to the db
				DocumentData newDocumentToAdd = new DocumentData();
				String msgbody = request.getReader().lines().collect(Collectors.joining());
				UTIL_HANDLER.serverlog("REQUEST BODY DATA:"+msgbody);
				newDocumentToAdd.setText_content_data(msgbody);
				newDocumentToAdd.setContent_type(requestContentType);
				//Add document data
				newDocumentID = DATA_SOURCE.addUpdateDocumentToDB(newDocumentToAdd);

			}else if(requestContentType.contains("multipart/form-data")) {
				Collection<Part> parts = request.getParts();
				// PrintWriter writer = response.getWriter();
				System.out.println("PARAMS:");
				for(Part part : parts) {
					if(part.getName().equalsIgnoreCase("DOCUMENT")) {
						FileInputStream fis =(FileInputStream) part.getInputStream();
						//add new document to the db
						DocumentData newDocumentToAdd = new DocumentData();
						newDocumentToAdd.setFilename(part.getSubmittedFileName());
						newDocumentToAdd.setDoc_file(fis);
						newDocumentToAdd.setContent_type(part.getContentType());
						newDocumentID = DATA_SOURCE.addUpdateDocumentToDB(newDocumentToAdd);
					}
				}
			}

			UTIL_HANDLER.serverlog("[processDocumentUploadRequest] DOCUMENT CREATED WITH ID:"+newDocumentID);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return newDocumentID;
	}



}

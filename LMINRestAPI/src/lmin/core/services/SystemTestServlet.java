package lmin.core.services;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class SystemTestServlet
 */
@WebServlet(name="SystemTestServlet", displayName="SystemTestServlet", urlPatterns = {"/systemtest"}, loadOnStartup = 1)
public class SystemTestServlet extends HttpServlet implements BaseServletInterface{
	private static final long serialVersionUID = 1L;
       

    public SystemTestServlet() {
        super();
    }


	public void init(ServletConfig config) throws ServletException {
		UTIL_HANDLER.serverlog("SystemTest Init!");	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String docID = UTIL_HANDLER.randomString();
		response.getWriter().append(" : Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
}

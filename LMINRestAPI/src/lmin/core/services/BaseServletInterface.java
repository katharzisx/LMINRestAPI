package lmin.core.services;

import javax.activation.DataHandler;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


import lmin.core.handlers.LMINDataHandler;
import lmin.core.handlers.UtilHandler;

public interface BaseServletInterface {
	//common to all SERVLETS/CONTROLLERS
	//version label
	String API_VERSION = "v1";
	//data source instance
	LMINDataHandler DATA_SOURCE = new LMINDataHandler();
	//Utilities handler
	UtilHandler UTIL_HANDLER = UtilHandler.getInstance();
}

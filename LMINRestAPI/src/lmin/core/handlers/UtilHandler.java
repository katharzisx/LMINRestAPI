package lmin.core.handlers;

import java.security.SecureRandom;
import java.util.Random;

import org.apache.log4j.Logger;

import lmin.core.services.BaseServletInterface;

public class UtilHandler {
	private static UtilHandler utilHandler = null;
	//logger instance
	static final Logger LOGGER = Logger.getLogger(UtilHandler.class);
	private UtilHandler() {};
	//Method to get instance for Utilhandler
	public static UtilHandler getInstance() {
		if(utilHandler==null) {
			utilHandler = new UtilHandler();
		}
		return utilHandler;
	}
	
	/**
	 * Doc types enum
	 * @author jeff
	 *
	 */
	public enum DOC_TYPES{
		TEXT,
		RTF,
		PDF,
		JPEG,
		PNG
	}
	
	/**
	 * System Server logger
	 * @param logMessage
	 */
	public void serverlog(String logMessage) {
		//LOGGER.debug(logMessage);
		//LOGGER.info(logMessage);
		System.out.println(logMessage);
	}
	
	/**
	 * 
	 * @param characterSet
	 * @param length
	 * @return
	 * Source: https://stackoverflow.com/questions/18069434/generating-alphanumeric-random-string-in-java
	 */
	public String randomString() {
		char[] characterSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		int length = 20;
	    Random random = new SecureRandom();
	    char[] result = new char[length];
	    for (int i = 0; i < result.length; i++) {
	        // picks a random index out of character set > random character
	        int randomCharIndex = random.nextInt(characterSet.length);
	        result[i] = characterSet[randomCharIndex];
	    }
	    return new String(result);
	}
}

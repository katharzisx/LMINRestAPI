package lmin.core.handlers;

import java.util.concurrent.ConcurrentHashMap;

import lmin.core.models.DocumentData;

public class LMINDataHandler {
	//Data-Stores
	static ConcurrentHashMap<String, ConcurrentHashMap<String, Object>> mapInMemoryDB = new ConcurrentHashMap<String, ConcurrentHashMap<String, Object>>();//Thread safe access per table/key
	//Will contain below tables
	ConcurrentHashMap<String, Object> mapDocumentTable = null;//Thread safe access per Document entry
	//Table Keys
	public static String DOCUMENT_TABLE = "DOCUMENT_TABLE";

	//Utililties handler
	UtilHandler UTIL = UtilHandler.getInstance();

	public LMINDataHandler() {
		UTIL.serverlog("DATA HANDLER CONSTRUCTOR!!!!");
	}

	public ConcurrentHashMap<String, ConcurrentHashMap<String, Object>> getDBAccess() {
		return mapInMemoryDB;
	}

	/**
	 * Add/Update New Document or Create doc table if not exists
	 * @param documentData
	 */
	public String addUpdateDocumentToDB(DocumentData documentData) {
		
		String documentId = documentData.getDoc_id();
		
		UTIL.serverlog("[addUpdateDocumentToDB] DOCUMENT ID:"+documentData.getDoc_id());
		//Check if Document Map/Table exists
		if (!mapInMemoryDB.containsKey(DOCUMENT_TABLE)){
			//create new Document Table
			mapDocumentTable =  new ConcurrentHashMap<String, Object>();
			//add table to mapInMemoryDB
			mapInMemoryDB.put(DOCUMENT_TABLE, mapDocumentTable);
		}
		//Check if DocumentData entry exists
		if(documentId==null || !mapDocumentTable.containsKey(documentData.getDoc_id())) {
			//Assign a random Alphanumeric ID to the new document
			String newDocID = UTIL.randomString();
			UTIL.serverlog("DOCUMENT ADD TO TABLE WITH ID:"+newDocID);
			documentData.setDoc_id(newDocID);
			//Add new entry
			mapDocumentTable.put(documentData.getDoc_id(), documentData);
			//Check if document added and return the id
			documentId = ((DocumentData)mapDocumentTable.get(newDocID)).getDoc_id();
		}else {
			//Update existing entry
			//add logic if more checks are required before updating
			mapDocumentTable.put(documentData.getDoc_id(), documentData);
		}
		
		return documentId;
	}
	
	
	/**
	 * Get DocumentData by ID
	 * @param documentId
	 * @return
	 */
	public DocumentData getDocumentByDocID(String documentId) {
		DocumentData resultDocument = null;
		if (mapInMemoryDB.containsKey(DOCUMENT_TABLE)){
			if(!mapDocumentTable.containsKey(documentId)) {
				resultDocument = (DocumentData)mapDocumentTable.get(documentId);
			}else {
				//DOCUMENTS NOT FOUND with given ID
				UTIL.serverlog("DOCUMENTS NOT FOUND with given ID: "+documentId);
			}
		}else {
			//NO DOCUMENTS IN DOC TABLE
			UTIL.serverlog("NO DOCUMENTS IN DOC TABLE!");
		}
		
		return resultDocument;
	}

}

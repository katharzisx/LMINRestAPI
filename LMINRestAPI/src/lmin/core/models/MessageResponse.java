package lmin.core.models;

public class MessageResponse {
	public MessageResponse(String message, int messageCode) {
		this.message = message;
		this.messageCode = messageCode;
	}

	private String message;
	private int messageCode;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(int messageCode) {
		this.messageCode = messageCode;
	}
}

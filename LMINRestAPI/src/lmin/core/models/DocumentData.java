package lmin.core.models;

import java.io.File;
import java.io.FileInputStream;

/**
 * Document Data
 * @author jeff
 *
 */
public class DocumentData {
	public DocumentData() {}
	private String doc_id;
	private String filename;
	private String content_type;
	private String text_content_data;
	
	private FileInputStream doc_file;
	
	
	

	
	//Setters and Getters
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getContent_type() {
		return content_type;
	}
	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}
	public String getText_content_data() {
		return text_content_data;
	}
	public void setText_content_data(String text_content_data) {
		this.text_content_data = text_content_data;
	}
	public FileInputStream getDoc_file() {
		return doc_file;
	}
	public void setDoc_file(FileInputStream doc_file) {
		this.doc_file = doc_file;
	}
	public String getDoc_id() {
		return doc_id;
	}
	public void setDoc_id(String doc_id) {
		this.doc_id = doc_id;
	}
	

}
